import React from 'react';
import HelloWorld from '@/components/HelloWorld';

function App(): React.ReactElement {
  return (
    <div className="app">
      React App
      <HelloWorld />
    </div>
  );
}

export default App;
